# Kotlin Spring Mongo

#### Run locally
```
git clone https://gitlab.com/hendisantika/kotlin-spring-mongo.git
```

```
gradle clean bootRun --info
```

#### Screen shot

##### Home Page

![Home Page](img/home.png "Home Page")

##### Add Data

![Add Data](img/add.png "Add Data")


